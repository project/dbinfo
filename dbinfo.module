<?php
/**
 * @file
 * Display additional active DB info in administrative reports.
 */


/**
 * Implements hook_theme_registry_alter().
 *
 * The ctools collapsible divs cannot be stacked without a CSS hack on top of
 * the default module behavior since the module currently does not use render
 * arrays to build its output, nor does it support additional $class values.
 *
 * We override the default functions behavior with our own function that inserts
 * a css class when has some css to fix the clearing issue.
 */
function dbinfo_theme_registry_alter(&$theme_registry) {
  $theme_registry['ctools_collapsible']['function'] = 'dbinfo_ctools_collapsible';
}


/**
 * Implements hook_menu().
 */
function dbinfo_menu() {
  $items['admin/config/system/dbinfo'] = array(
    'title' => 'Database Status Information Settings',
    'description' => 'Change whether the status report attempts to verify every database connection.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dbinfo_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'dbinfo.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}


/**
 * Miscellanous module functions.
 */


/**
 * Overriding implementation of theme_ctools_collapsible().
 *
 * Unable to cleany inject some additional CSS to fix having these stack one
 * after another with original ctools function returning a string and not
 * accepting $class anywhere.
 */
function dbinfo_ctools_collapsible($vars) {
  ctools_add_js('collapsible-div');
  ctools_add_css('collapsible-div');

  $dbinfo_fix_seq_class = '';
  if (current_path() === 'admin/reports/status') {
    drupal_add_css(drupal_get_path('module', 'dbinfo') . '/css/dbinfo.css', array('group' => CSS_DEFAULT, 'every_page' => FALSE));
    $dbinfo_fix_seq_class = ' dbinfo-fix-seq';
  }

  $class = $vars['collapsed'] ? ' ctools-collapsed' : '';
  $output = '<div class="ctools-collapsible-container' . $class . $dbinfo_fix_seq_class . '" >';
  $output .= '<div class="ctools-collapsible-handle">' . $vars['handle'] . '</div>';
  $output .= '<div class="ctools-collapsible-content">' . $vars['content'] . '</div>';
  $output .= '</div>';

  return $output;
}
